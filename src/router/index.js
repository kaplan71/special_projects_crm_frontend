import Vue from 'vue';
import VueRouter from 'vue-router';

import ProjectDetailView from '@/views/projects/ProjectDetailView';
import ProjectListView from '@/views/projects/ProjectsListView';
import LoginView from '@/views/auth/LoginView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login/',
    name: 'login',
    component: LoginView,
  },
  {
    path: '/:queryString?',
    name: 'main',
    component: ProjectListView,
  },
  {
    path: '/projects/:id/',
    name: 'project',
    component: ProjectDetailView,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
