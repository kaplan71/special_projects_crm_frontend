import ax from 'axios';
import router from '@/router';
import store from '@/store';

ax.defaults.withCredentials = true;
ax.defaults.headers = {
  'Content-Type': 'application/json',
};
ax.interceptors.response.use(
  (response) => {
    return Promise.resolve(response);
  },
  (error) => {
    if (error.response.status === 403) {
      store.commit('auth/setIsAuth', false);
      store.commit('auth/setUser', { email: '', firstName: '' });
      try {
        router.push('/login/');
      } catch {
        1;
      }
    }
    return Promise.reject(error);
  }
);

export const axios = ax;
export default {
  install(Vue) {
    Vue.prototype.$axios = ax;
  },
};
