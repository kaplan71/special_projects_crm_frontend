import Vue from 'vue';
import Vuex from 'vuex';
import { axios } from '@/plugins/axios';

import auth from '@/store/modules/auth';
import projects from '@/store/modules/projects';

Vue.use(Vuex);

const axiosPlugin = (store) => {
  store.$axios = axios;
};

export default new Vuex.Store({
  plugins: [axiosPlugin],
  modules: {
    auth: auth,
    projects: projects,
  },
});
