export default {
  setProjectsList(state, payload) {
    state.projects = payload;
  },
  setProjectDetail(state, payload) {
    state.project = payload;
  },
  setPage(state, payload) {
    state.projectsPagination.page = payload;
  },
  setItemPerPage(state, payload) {
    state.projectsPagination.itemPerPage = payload;
  },
  setCount(state, payload) {
    state.projectsPagination.count = payload;
  },
  setPages(state, payload) {
    state.projectsPagination.pages = payload;
  },
  setPlatforms(state, payload) {
    state.platforms = payload;
  },
  setTypes(state, payload) {
    state.types = payload;
  },
  setManagers(state, payload) {
    state.managers = payload;
  },
  setCategories(state, payload) {
    state.categories = payload;
  },
  setSales(state, payload) {
    state.sales = payload;
  },
};
