export default {
  async getProjectsList({ commit, state }, queryString) {
    const { itemPerPage: pageSize } = state.projectsPagination;
    const { page: page } = state.projectsPagination;

    try {
      const { data } = await this.$axios.get(
        `/api/v1/projects/list/?page=${page}&page_size=${pageSize}&${
          queryString || ''
        }`
      );
      commit('setProjectsList', data.results);
      commit('setCount', data.count);
      commit('setPages', data.pages);
    } catch (error) {
      throw Error(error);
    }
  },
  async getProject({ commit }, id) {
    try {
      const { data } = await this.$axios.get(`/api/v1/projects/detail/${id}`);
      commit('setProjectDetail', data);
    } catch (error) {
      throw Error(error);
    }
  },
  async getPlatforms({ commit }) {
    try {
      const { data } = await this.$axios.get('/api/v1/projects/platforms/');
      commit('setPlatforms', data);
    } catch (error) {
      throw Error(error);
    }
  },
  async getTypes({ commit }) {
    try {
      const { data } = await this.$axios.get('/api/v1/projects/types/');
      commit('setTypes', data);
    } catch (error) {
      throw Error(error);
    }
  },
  async getManagers({ commit }) {
    try {
      const { data } = await this.$axios.get('/api/v1/projects/managers/');
      commit('setManagers', data);
    } catch (error) {
      throw Error(error);
    }
  },
  async getCategories({ commit }) {
    try {
      const { data } = await this.$axios.get('/api/v1/projects/categories/');
      commit('setCategories', data);
    } catch (error) {
      throw Error(error);
    }
  },
  async getSales({ commit }) {
    try {
      const { data } = await this.$axios.get('/api/v1/projects/sales/');
      commit('setSales', data);
    } catch (error) {
      throw Error(error);
    }
  },
};
