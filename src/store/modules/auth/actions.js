export default {
  async login({ commit }, credentials) {
    try {
      const { data } = await this.$axios.post('/api/v1/auth/login/', {
        ...credentials,
      });
      commit('setUser', data);
      commit('setIsAuth');
    } catch (e) {
      commit('setIsAuth', false);
      throw e;
    }
  },
  async logout({ commit }) {
    try {
      await this.$axios.post('/api/v1/auth/logout/');
      commit('setIsAuth', false);
    } catch (e) {
      commit('setIsAuth', false);
      throw Error(e);
    }
  },
  async checkUser({ commit }) {
    try {
      const { data } = await this.$axios.post('/api/v1/auth/check/');
      commit('setUser', data);
      commit('setIsAuth', true);
    } catch (e) {
      commit('setIsAuth', false);
      throw Error(e);
    }
  },
};
