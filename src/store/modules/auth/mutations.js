export default {
  setUser(state, payload) {
    state.email = payload.email;
    state.firstName = payload.firstName;
  },
  setIsAuth(state, payload = true) {
    state.isAuth = payload;
  },
};
